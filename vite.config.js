import {defineConfig} from "vite"
import symfonyPlugin from "vite-plugin-symfony"
import WindiCSS from 'vite-plugin-windicss'
import FullReload from 'vite-plugin-full-reload'

/* if you're using React */
// import react from '@vitejs/plugin-react';

export default defineConfig({
    plugins: [
        /* react(), // if you're using React */
        symfonyPlugin(),
        WindiCSS(),
        FullReload(['templates/**/*'])
    ],
    build: {
        rollupOptions: {
            input: {
                app: "./assets/app.js"
            },
        },
    },
});
