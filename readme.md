# Test app (symfony6.2 + easyAdmin)

#### Requirements: php >= 8.1, postgresql, docker

1. composer install
2. docker compose up -d
3. migrate
4. npm i
5. npm run dev
6. symfony serve

Create user from console: <b>symfony console adduser admin@test.com pass123 --admin</b>

На symfony ранее не писал. Затратил времени (с изучением документации): 18-20 часов.

Пункт 3 "возможность отслеживать события привязанные к одному пользователю" не совсем понял каким образом - добавил
event и subscriber
App\Subscriber\EventPublishSubscriber.

По тестам хотел что-то попробовать написать но упираюсь по времени. 