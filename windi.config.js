import {defineConfig} from 'windicss/helpers'

export default defineConfig({
    extract: {
        include: ['templates/**/*.twig'],
        exclude: ['node_modules', '.git', 'var', 'src'],
    },
    plugins: [require('daisyui')],
    daisyui: {
        styled: true,
        themes: [
            [
                {
                    "primary": "#570DF8",
                    "secondary": "#F000B8",
                    "accent": "#37CDBE",
                    "neutral": "#111111",
                    "base-100": "#FFFFFF",
                    "info": "#3ABFF8",
                    "success": "#36D399",
                    "warning": "#FBBD23",
                    "error": "#F87272",
                }
            ],
        ],
        base: true,
        utils: true,
        logs: true,
        rtl: false,
        prefix: "",
        darkTheme: false,
    },
})