<?php

namespace App\Event;

use Doctrine\Common\Collections\Collection;
use Symfony\Contracts\EventDispatcher\Event;

class EventPublishEvent extends Event
{
    private \App\Entity\Event $_event;
    private Collection $_members;

    const NAME = 'event.published';

    public function __construct(\App\Entity\Event $event)
    {
        $this->_event = $event;
    }

    public function getEvent(): \App\Entity\Event
    {
        return $this->_event;
    }

    public function getMembers(): Collection
    {
        return $this->_event->getMembers();
    }
}