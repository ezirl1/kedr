<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterController extends AbstractController
{
    #[Route('/register', name: 'app_register', methods: ['POST', 'GET'])]
    public function index(Request                     $request,
                          UserPasswordHasherInterface $hasher,
                          ManagerRegistry             $doctrine,
                          ValidatorInterface          $validator,
                          Security                    $security): Response
    {
        if ($request->getMethod() == 'GET') {
            return $this->redirectToRoute('app_login');
        }

        $user = new User($hasher);
        $user->setEmail($request->get('email'));
        $user->setPassword($request->get('password'));
        $user->setConfirmPassword($request->get('re-password'));

        $errors = $validator->validate($user);

        if (count($errors)) {
            return $this->render('security/login.html.twig', ['errors' => $errors, 'last_username' => $user->getEmail(), 'error' => null]);
        }

        $user->hashPassword();
        $userRep = new UserRepository($doctrine);
        $userRep->save($user, true);


        $security->login($user);
        return $this->redirectToRoute('home');
    }
}
