<?php

namespace App\Controller\Admin;

use App\Entity\Event;
use App\Event\EventPublishEvent;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class EventCrudController extends AbstractCrudController
{
    private EventDispatcherInterface $dispatcher;
    private Event $event;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public static function getEntityFqcn(): string
    {
        return Event::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextEditorField::new('data'),
            AssociationField::new('members', 'Users'),
            DateTimeField::new('start', 'Start time'),
            DateTimeField::new('finish', 'End time'),
        ];
    }

    protected function getRedirectResponseAfterSave(AdminContext $context, string $action): RedirectResponse
    {
        $event = new EventPublishEvent($context->getEntity()->getInstance());
        $this->dispatcher->dispatch($event, EventPublishEvent::NAME);
        return parent::getRedirectResponseAfterSave($context, $action);
    }
}
