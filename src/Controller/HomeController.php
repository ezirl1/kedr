<?php

namespace App\Controller;

use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', 'home')]
    public function index(EventRepository $eventRepository)
    {
        $events = $eventRepository->findAllAndGroupByDate();
        ksort($events);
        return $this->render('home.twig', ['groupedEvents' => $events]);
    }
}