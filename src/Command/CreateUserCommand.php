<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[AsCommand(
    name: 'CreateUser',
    description: 'Create user from console',
    aliases: ['app:add-user', 'app:create-user', 'adduser']
)]
class CreateUserCommand extends Command
{
    protected ManagerRegistry $doctrine;
    protected UserPasswordHasherInterface $passwordHasher;

    public function __construct(ManagerRegistry $doctrine, UserPasswordHasherInterface $passwordHasher, string $name = null)
    {
        parent::__construct($name);
        $this->doctrine = $doctrine;
        $this->passwordHasher = $passwordHasher;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'User email')
            ->addOption('admin', 'a', InputOption::VALUE_OPTIONAL, 'Set admin role to user')
            ->addArgument('password', InputArgument::REQUIRED, 'User password');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $user = new User($this->passwordHasher);
        $user->setEmail($input->getArgument('email'));
        $user->setPassword($input->getArgument('password'));
        $user->hashPassword();
        if ($input->hasOption('admin')) {
            $user->setRoles(['ROLE_ADMIN']);
        }

        $userRepository = new UserRepository($this->doctrine);
        $userRepository->save($user, true);

        $output->writeln('Email: ' . $input->getArgument('email'));
        $output->writeln('Password: ' . $input->getArgument('password'));
        $output->writeln('Role: ' . ($input->hasOption('admin') ? 'admin' : 'user'));

        $io->success('User created.');

        return Command::SUCCESS;
    }
}
