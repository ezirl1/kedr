<?php

namespace App\Subscriber;

use App\Event\EventPublishEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EventPublishSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return [
            EventPublishEvent::NAME => ['onEventPublish', 1]
        ];
    }

    public function onEventPublish(EventPublishEvent $event)
    {
        // send notification to users
    }

}